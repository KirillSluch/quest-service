FROM golang:alpine AS builder

WORKDIR /build

COPY . .

RUN go mod download

RUN go build -o quest-service ./cmd/app/main.go

FROM alpine

WORKDIR /build

COPY --from=builder /build/quest-service /build/quest-service
COPY --from=builder /build/config/docker-config.yaml /build/config.yaml

ENV CONFIG_PATH="/build/config.yaml"

CMD ["/build/quest-service"]

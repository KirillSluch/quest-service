package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"task-manager/internal/app"
	"time"
)

func main() {
	ctx := context.Background()

	a, err := app.NewApp(ctx)
	if err != nil {
		log.Fatalf("failed to init app: %s", err.Error())
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	err = a.Run()
	if err != nil {
		log.Fatalf("failed to run app: %s", err.Error())
	}

	<-done

	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	err = a.Stop(ctx)
	if err != nil {
		log.Fatalf("failed to run stop: %s", err.Error())
	}
	log.Println("server stopped")
}

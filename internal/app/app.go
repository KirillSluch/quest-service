package app

import (
	"context"
	"errors"
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
	"task-manager/internal/http-server/handlers/history"
	"task-manager/internal/http-server/handlers/quests"
	"task-manager/internal/http-server/handlers/register"
	"task-manager/internal/http-server/handlers/users"
	"task-manager/internal/http-server/middleware/auth"
)

type App struct {
	serviceProvider *serviceProvider
	httpServer      *http.Server
}

// NewApp init dependencies and application
func NewApp(ctx context.Context) (*App, error) {
	a := &App{}

	err := a.initDeps(ctx)
	if err != nil {
		return nil, err
	}

	return a, nil
}

// Run application
func (a *App) Run() error {
	return a.runHTTPServer()
}

// Stop application, services and close connections
func (a *App) Stop(ctx context.Context) error {
	if err := a.stopHTTPServer(ctx); err != nil {
		return err
	}

	log.Println("stopping services...")

	return a.serviceProvider.Finish()
}

func (a *App) initDeps(ctx context.Context) error {
	inits := []func(context.Context) error{
		a.initServiceProvider,
		a.initHTTPServer,
	}

	for _, f := range inits {
		err := f(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *App) initServiceProvider(_ context.Context) error {
	a.serviceProvider = newServiceProvider()
	return nil
}

func (a *App) initHTTPServer(ctx context.Context) error {
	router := chi.NewRouter()

	router.Use(auth.NewApiToken(a.serviceProvider.Config().API.Key))

	router.Post("/quest", quests.New(ctx, a.serviceProvider.QuestService(ctx)))
	router.Post("/user", users.New(ctx, a.serviceProvider.UserService(ctx)))
	router.Get("/user/{user_id}/history", history.New(ctx, a.serviceProvider.UserService(ctx)))
	router.Post("/user/{user_id}/quest/{quest_id}", register.New(ctx, a.serviceProvider.UserService(ctx)))

	a.httpServer = &http.Server{
		Addr:    a.serviceProvider.Config().HTTPServer.Address,
		Handler: router,
	}

	return nil
}

func (a *App) runHTTPServer() error {
	log.Printf("server is running on %s", a.serviceProvider.Config().HTTPServer.Address)

	go func() {
		if err := a.httpServer.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Fatalln("failed to start http server")
			}
		}
	}()

	return nil
}

func (a *App) stopHTTPServer(ctx context.Context) error {
	log.Println("stopping http server...")

	if err := a.httpServer.Shutdown(ctx); err != nil {

		return errors.New("failed to stop server")
	}

	return nil
}

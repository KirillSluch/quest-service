package app

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
	"task-manager/internal/config"
	"task-manager/internal/service"
	questService "task-manager/internal/service/quest"
	userService "task-manager/internal/service/user"
	"task-manager/internal/storage"
	"task-manager/internal/storage/quest"
	"task-manager/internal/storage/user"
)

// serviceProvider helps to init and use dependencies. Primitive DI-container
type serviceProvider struct {
	config *config.Config

	userRepository  storage.UserRepository
	questRepository storage.QuestRepository

	userService  service.UserService
	questService service.QuestService

	pool *pgxpool.Pool
}

func newServiceProvider() *serviceProvider {
	return &serviceProvider{}
}

func (provider *serviceProvider) Config() *config.Config {
	if provider.config == nil {
		cfg, err := config.Load()
		if err != nil {
			log.Fatalf("failed to get config: %s", err.Error())
		}

		provider.config = cfg
	}
	return provider.config
}

func (provider *serviceProvider) PgxPool() *pgxpool.Pool {
	if provider.pool == nil {
		pool, err := pgxpool.New(context.Background(), provider.Config().StoragePath)
		if err != nil {
			log.Fatalf("Unable to create connection pool: %s\n", err.Error())
		}
		provider.pool = pool
	}
	return provider.pool
}

func (provider *serviceProvider) UserRepository(ctx context.Context) storage.UserRepository {
	if provider.userRepository == nil {
		repo, err := user.NewRepository(ctx, provider.PgxPool())
		if err != nil {
			log.Fatalf("failed to init user repository: %s\n", err.Error())
		}

		provider.userRepository = repo
	}
	return provider.userRepository
}

func (provider *serviceProvider) QuestRepository(ctx context.Context) storage.QuestRepository {
	if provider.questRepository == nil {
		repo, err := quest.NewRepository(ctx, provider.PgxPool())
		if err != nil {
			log.Fatalf("failed to init quest repository: %s\n", err.Error())
		}

		provider.questRepository = repo
	}
	return provider.questRepository
}

func (provider *serviceProvider) UserService(ctx context.Context) service.UserService {
	if provider.userService == nil {
		srv := userService.NewService(provider.UserRepository(ctx))
		provider.userService = srv
	}

	return provider.userService
}

func (provider *serviceProvider) QuestService(ctx context.Context) service.QuestService {
	if provider.questService == nil {
		srv := questService.NewService(provider.QuestRepository(ctx))
		provider.questService = srv
	}

	return provider.questService
}

func (provider *serviceProvider) Finish() error {
	provider.PgxPool().Close()
	return nil
}

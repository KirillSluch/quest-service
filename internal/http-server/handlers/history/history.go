package history

import (
	"context"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"log"
	"net/http"
	"task-manager/internal/model"
	"task-manager/internal/service"
)

func New(ctx context.Context, userService service.UserService) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		userId := chi.URLParam(request, "user_id")
		if userId == "" {
			log.Println("failed to get user id from url")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "bad request",
			})
			return
		}

		statistics, err := userService.GetStatistics(ctx, userId)

		if err != nil {
			if errors.Is(err, service.ErrUserNotFound) {
				log.Printf("user not found: %s\n", err.Error())

				render.Status(request, http.StatusNotFound)
				render.JSON(writer, request, model.Response{
					Status: http.StatusNotFound,
					Error:  "user not found",
				})
				return
			}

			log.Printf("failed to get statistics: %s\n", err.Error())

			render.Status(request, http.StatusInternalServerError)
			render.JSON(writer, request, model.Response{
				Status: http.StatusInternalServerError,
				Error:  "failed to get statistics",
			})
			return
		}

		log.Printf("get statistic: %s\n", userId)
		render.Status(request, http.StatusOK)
		render.JSON(writer, request, model.ResponseOk(http.StatusOK, statistics))
	}
}

package history

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"task-manager/internal/model"
	"task-manager/internal/service"
	"task-manager/internal/service/mocks"
	"testing"
)

func TestRegisterHandler(t *testing.T) {
	cases := []struct {
		name            string
		userId          string
		userName        string
		balance         int
		respError       string
		mockError       error
		expectingStatus int
	}{
		{
			name:            "Success",
			userId:          uuid.New().String(),
			userName:        "Kirill",
			balance:         50,
			expectingStatus: http.StatusOK,
		},
		{
			name:            "User Id Validation Failed",
			userId:          "",
			expectingStatus: http.StatusBadRequest,
		},
		{
			name:            "User Not Found",
			userId:          uuid.New().String(),
			expectingStatus: http.StatusNotFound,
			mockError:       service.ErrUserNotFound,
			respError:       "user not found",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userServiceMock := mocks.NewUserService(t)
			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				userServiceMock.
					On(
						"GetStatistics",
						context.Background(),
						testCase.userId,
					).
					Return(
						&model.Statistic{
							User: model.User{
								ID:      testCase.userName,
								Name:    testCase.name,
								Balance: testCase.balance,
							},
							SolvedQuests: nil,
						},
						nil,
					).
					Once()
			} else if testCase.mockError != nil || testCase.respError != "" {
				userServiceMock.
					On(
						"GetStatistics",
						context.Background(),
						testCase.userId,
					).
					Return(
						nil,
						testCase.mockError,
					).
					Once()
			}

			handler := New(context.Background(), userServiceMock)

			w := httptest.NewRecorder()
			r := httptest.NewRequest("GET", "/user/{user_id}/history", nil)

			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("user_id", testCase.userId)

			r = r.WithContext(context.WithValue(r.Context(), chi.RouteCtxKey, rctx))

			handler(w, r)

			require.Equal(t, w.Code, testCase.expectingStatus)

			body := w.Body.String()

			if testCase.respError == "" {
				var resp model.Statistic

				require.NoError(t, json.Unmarshal([]byte(body), &resp))
			} else {
				var resp struct {
					Status int
					Error  string
				}
				require.NoError(t, json.Unmarshal([]byte(body), &resp))

				require.Equal(t, resp.Error, testCase.respError)
			}

		})
	}
}

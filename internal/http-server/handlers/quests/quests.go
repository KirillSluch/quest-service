package quests

import (
	"context"
	"encoding/json"
	"github.com/go-chi/render"
	"io"
	"log"
	"net/http"
	"task-manager/internal/model"
	"task-manager/internal/service"
)

func New(ctx context.Context, questService service.QuestService) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		var (
			newQuest model.Quest
			body     []byte
		)

		body, err := io.ReadAll(request.Body)
		if err != nil {
			log.Println("wrong request body")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "wrong request body",
			})
			return
		}

		err = json.Unmarshal(body, &newQuest)
		if err != nil {
			log.Println("failed to parse body")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "failed to parse body",
			})
			return
		}

		if newQuest.Name == "" || newQuest.Cost < 0 {
			log.Println("validation error")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "quest name can not be empty and cost must be greater or equal zero",
			})
			return
		}

		quest, err := questService.Create(ctx, &newQuest)
		if err != nil {
			log.Printf("failed to create quest: %s\n", err.Error())

			render.Status(request, http.StatusInternalServerError)
			render.JSON(writer, request, model.Response{
				Status: http.StatusInternalServerError,
				Error:  "failed to create quest",
			})
			return
		}

		log.Printf("created quest: %s\n", quest.ID)
		render.Status(request, http.StatusCreated)
		render.JSON(writer, request, model.ResponseOk(http.StatusCreated, quest))
	}
}

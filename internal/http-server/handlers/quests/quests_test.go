package quests

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"task-manager/internal/model"
	"task-manager/internal/service/mocks"
	"testing"
)

func TestQuestsHandler(t *testing.T) {
	cases := []struct {
		name            string
		questName       string
		cost            int
		respError       string
		mockError       error
		expectingStatus int
	}{
		{
			name:            "Success",
			questName:       "Testing Quest",
			cost:            50,
			expectingStatus: http.StatusCreated,
		},
		{
			name:            "Quest Name Validation Failed",
			questName:       "",
			cost:            0,
			expectingStatus: http.StatusBadRequest,
			respError:       "quest name can not be empty and cost must be greater or equal zero",
		},
		{
			name:            "Quest Cost Validation Failed",
			questName:       "Test",
			cost:            -10,
			expectingStatus: http.StatusBadRequest,
			respError:       "quest name can not be empty and cost must be greater or equal zero",
		},
		{
			name:            "Service Error",
			questName:       "Testing Quest",
			cost:            50,
			expectingStatus: http.StatusInternalServerError,
			mockError:       errors.New("service failed"),
			respError:       "failed to create quest",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			questServiceMock := mocks.NewQuestService(t)
			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				questServiceMock.
					On(
						"Create",
						context.Background(),
						&model.Quest{
							Name: testCase.questName,
							Cost: testCase.cost,
						}).
					Return(
						&model.Quest{
							ID:   uuid.New().String(),
							Name: testCase.questName,
							Cost: testCase.cost,
						}, nil).
					Once()
			} else if testCase.mockError != nil {
				questServiceMock.On(
					"Create",
					context.Background(),
					&model.Quest{
						Name: testCase.questName,
						Cost: testCase.cost,
					}).Return(nil, testCase.mockError)
			}

			handler := New(context.Background(), questServiceMock)

			input := fmt.Sprintf(`{"name": "%s", "cost": %d}`, testCase.questName, testCase.cost)
			req, err := http.NewRequest(http.MethodPost, "/quest", bytes.NewReader([]byte(input)))
			require.NoError(t, err)

			rr := httptest.NewRecorder()
			handler.ServeHTTP(rr, req)

			require.Equal(t, rr.Code, testCase.expectingStatus)

			body := rr.Body.String()

			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				var resp model.User

				require.NoError(t, json.Unmarshal([]byte(body), &resp))
			} else {
				var resp struct {
					Error  string
					Status int
				}
				require.NoError(t, json.Unmarshal([]byte(body), &resp))

				require.Equal(t, resp.Error, testCase.respError)
				require.Equal(t, resp.Status, testCase.expectingStatus)
			}
		})
	}
}

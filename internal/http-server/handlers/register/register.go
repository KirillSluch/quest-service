package register

import (
	"context"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"log"
	"net/http"
	"task-manager/internal/model"
	"task-manager/internal/service"
)

func New(ctx context.Context, userService service.UserService) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		userId := chi.URLParam(request, "user_id")
		if userId == "" {
			log.Println("failed to get user id from url")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "bad request",
			})
			return
		}

		questId := chi.URLParam(request, "quest_id")
		if questId == "" {
			log.Println("failed to get quest id from url")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "bad request",
			})
			return
		}

		err := userService.RegisterQuest(ctx, userId, questId)

		if err != nil {
			if errors.Is(err, service.ErrUserOrQuestNotFound) {
				log.Printf("user<%s> or quest<%s> not found", userId, questId)

				render.Status(request, http.StatusNotFound)
				render.JSON(writer, request, model.Response{
					Status: http.StatusNotFound,
					Error:  err.Error(),
				})
				return
			} else if errors.Is(err, service.ErrQuestAlreadyCompleted) {
				log.Printf("user<%s> already compleate quest<%s>", userId, questId)

				render.Status(request, http.StatusConflict)
				render.JSON(writer, request, model.Response{
					Status: http.StatusConflict,
					Error:  err.Error(),
				})
				return
			}

			log.Printf("failed to create statistic: %s\n", err.Error())

			render.Status(request, http.StatusInternalServerError)
			render.JSON(writer, request, model.Response{
				Status: http.StatusInternalServerError,
				Error:  "failed to save statistics",
			})
			return
		}

		log.Printf("update statistic: %s\n", userId)
		render.Status(request, http.StatusAccepted)
		render.JSON(writer, request, model.Response{Status: http.StatusAccepted})
	}
}

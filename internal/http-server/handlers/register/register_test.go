package register

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"task-manager/internal/service"
	"task-manager/internal/service/mocks"
	"testing"
)

func TestRegisterHandler(t *testing.T) {
	cases := []struct {
		name            string
		userId          string
		questId         string
		respError       string
		mockError       error
		expectingStatus int
	}{
		{
			name:            "Success",
			userId:          uuid.New().String(),
			questId:         uuid.New().String(),
			expectingStatus: http.StatusAccepted,
		},
		{
			name:            "User Id Validation Failed",
			userId:          "",
			questId:         uuid.New().String(),
			expectingStatus: http.StatusBadRequest,
		},
		{
			name:            "Quest Id Validation Failed",
			userId:          uuid.New().String(),
			questId:         "",
			expectingStatus: http.StatusBadRequest,
		},
		{
			name:            "User Or Quest Not Found",
			userId:          uuid.New().String(),
			questId:         uuid.New().String(),
			expectingStatus: http.StatusNotFound,
			mockError:       service.ErrUserOrQuestNotFound,
			respError:       service.ErrUserOrQuestNotFound.Error(),
		},
		{
			name:            "Quest Already Completed",
			userId:          uuid.New().String(),
			questId:         uuid.New().String(),
			expectingStatus: http.StatusConflict,
			mockError:       service.ErrQuestAlreadyCompleted,
			respError:       service.ErrQuestAlreadyCompleted.Error(),
		},
		{
			name:            "DB failed",
			userId:          uuid.New().String(),
			questId:         uuid.New().String(),
			expectingStatus: http.StatusInternalServerError,
			mockError:       errors.New("internal server error"),
			respError:       "failed to save statistics",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userServiceMock := mocks.NewUserService(t)
			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				userServiceMock.
					On(
						"RegisterQuest",
						context.Background(),
						testCase.userId,
						testCase.questId,
					).
					Return(nil).
					Once()
			} else if testCase.mockError != nil || testCase.respError != "" {
				userServiceMock.
					On(
						"RegisterQuest",
						context.Background(),
						testCase.userId,
						testCase.questId,
					).
					Return(
						testCase.mockError,
					).
					Once()
			}

			handler := New(context.Background(), userServiceMock)

			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "/user/{user_id}/quest/{quest_id}", nil)

			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("user_id", testCase.userId)
			rctx.URLParams.Add("quest_id", testCase.questId)

			r = r.WithContext(context.WithValue(r.Context(), chi.RouteCtxKey, rctx))

			handler(w, r)

			require.Equal(t, w.Code, testCase.expectingStatus)

			if testCase.respError != "" {
				body := w.Body.String()
				var resp struct {
					Status int
					Error  string
				}
				require.NoError(t, json.Unmarshal([]byte(body), &resp))

				require.Equal(t, resp.Error, testCase.respError)
			}

		})
	}
}

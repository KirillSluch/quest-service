package users

import (
	"context"
	"encoding/json"
	"github.com/go-chi/render"
	"io"
	"log"
	"net/http"
	"task-manager/internal/model"
	"task-manager/internal/service"
)

func New(ctx context.Context, userService service.UserService) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		var (
			newUser model.User
			body    []byte
		)

		body, err := io.ReadAll(request.Body)
		if err != nil {
			log.Println("wrong request body")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "wrong request body",
			})
			return
		}

		err = json.Unmarshal(body, &newUser)
		if err != nil {
			log.Println("failed to parse body")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "failed to parse body",
			})
			return
		}

		if newUser.Name == "" {
			log.Println("validation error")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "user name can not be empty",
			})
			return
		}

		if newUser.Balance < 0 {
			log.Println("validation error")

			render.Status(request, http.StatusBadRequest)
			render.JSON(writer, request, model.Response{
				Status: http.StatusBadRequest,
				Error:  "user balance can not be negative",
			})
			return
		}

		user, err := userService.Create(ctx, &newUser)
		if err != nil {
			log.Printf("failed to create user: %s\n", err.Error())

			render.Status(request, http.StatusInternalServerError)
			render.JSON(writer, request, model.Response{
				Status: http.StatusInternalServerError,
				Error:  "failed to create user",
			})
			return
		}

		render.Status(request, http.StatusCreated)
		log.Printf("created user: %s\n", user.ID)
		render.JSON(writer, request, model.ResponseOk(http.StatusCreated, user))
	}
}

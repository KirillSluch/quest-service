package users

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"task-manager/internal/model"
	"task-manager/internal/service/mocks"
	"testing"
)

func TestUsersHandler(t *testing.T) {
	cases := []struct {
		name            string
		userName        string
		balance         int
		respError       string
		mockError       error
		expectingStatus int
	}{
		{
			name:            "Success",
			userName:        "Kirill",
			balance:         0,
			expectingStatus: http.StatusCreated,
		},
		{
			name:            "Username Validation Failed",
			userName:        "",
			balance:         0,
			expectingStatus: http.StatusBadRequest,
			respError:       "user name can not be empty",
		},
		{
			name:            "User Balance Validation Failed",
			userName:        "Testname",
			balance:         -10,
			expectingStatus: http.StatusBadRequest,
			respError:       "user balance can not be negative",
		},
		{
			name:            "Service Error",
			userName:        "Testname",
			balance:         0,
			expectingStatus: http.StatusInternalServerError,
			mockError:       errors.New("some service error"),
			respError:       "failed to create user",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userServiceMock := mocks.NewUserService(t)
			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				userServiceMock.
					On(
						"Create",
						context.Background(),
						&model.User{
							Name:    testCase.userName,
							Balance: testCase.balance,
						}).
					Return(
						&model.User{
							ID:      uuid.New().String(),
							Name:    testCase.userName,
							Balance: testCase.balance,
						}, nil).
					Once()
			} else if testCase.mockError != nil {
				userServiceMock.On(
					"Create",
					context.Background(),
					&model.User{
						Name:    testCase.userName,
						Balance: testCase.balance,
					}).Return(nil, testCase.mockError)
			}

			handler := New(context.Background(), userServiceMock)

			input := fmt.Sprintf(`{"name": "%s", "balance": %d}`, testCase.userName, testCase.balance)
			req, err := http.NewRequest(http.MethodPost, "/user", bytes.NewReader([]byte(input)))
			require.NoError(t, err)

			rr := httptest.NewRecorder()
			handler.ServeHTTP(rr, req)

			require.Equal(t, rr.Code, testCase.expectingStatus)

			body := rr.Body.String()

			if testCase.expectingStatus >= 200 && testCase.expectingStatus < 300 {
				var resp model.User

				require.NoError(t, json.Unmarshal([]byte(body), &resp))
			} else {
				var resp struct {
					Error  string
					Status int
				}
				require.NoError(t, json.Unmarshal([]byte(body), &resp))

				require.Equal(t, resp.Error, testCase.respError)
				require.Equal(t, resp.Status, testCase.expectingStatus)
			}

		})
	}
}

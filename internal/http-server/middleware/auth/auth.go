package auth

import (
	"github.com/go-chi/render"
	"log"
	"net/http"
	"task-manager/internal/model"
)

func NewApiToken(validToken string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodPost {
				token := r.Header.Get("Authorization")
				if token == "" {
					log.Println("no creds found")

					render.Status(r, http.StatusUnauthorized)
					render.JSON(w, r, model.Response{
						Status: http.StatusUnauthorized,
						Error:  "no creds found",
					})

					return
				} else if validToken != token {
					log.Println("invalid creds")

					render.Status(r, http.StatusForbidden)
					render.JSON(w, r, model.Response{
						Status: http.StatusForbidden,
						Error:  "invalid creds",
					})
					return
				}
			}

			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}

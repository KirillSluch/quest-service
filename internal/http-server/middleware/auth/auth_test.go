package auth

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAuthMiddleware(t *testing.T) {
	cases := []struct {
		name            string
		token           string
		path            string
		method          string
		respError       string
		expectingStatus int
		noCreds         bool
	}{
		{
			name:            "Success",
			token:           "qwerty007",
			path:            "/user",
			method:          "POST",
			expectingStatus: http.StatusOK,
		},
		{
			name:            "No Creds Found",
			token:           "",
			path:            "/user",
			method:          "POST",
			respError:       "no creds found",
			expectingStatus: http.StatusUnauthorized,
		},
		{
			name:            "GET Without Token",
			path:            "/user",
			method:          "GET",
			expectingStatus: http.StatusOK,
		},
		{
			name:            "GET With Token",
			token:           "qwerty007",
			path:            "/user",
			method:          "GET",
			expectingStatus: http.StatusOK,
		},
		{
			name:            "Forbidden",
			token:           "qwerty007",
			path:            "/user",
			method:          "POST",
			respError:       "invalid creds",
			expectingStatus: http.StatusForbidden,
		},
		{
			name:            "No Creds Header",
			token:           "qwerty007",
			path:            "/user",
			method:          "POST",
			respError:       "no creds found",
			noCreds:         true,
			expectingStatus: http.StatusUnauthorized,
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				r.Response = &http.Response{StatusCode: http.StatusOK}
			})

			token := testCase.token
			if testCase.expectingStatus == http.StatusForbidden {
				token += "5"
			}
			handler := NewApiToken(token)
			w := httptest.NewRecorder()
			r := httptest.NewRequest(testCase.method, testCase.path, nil)

			if !testCase.noCreds {
				r.Header.Add("Authorization", testCase.token)
			}

			handler(nextHandler).ServeHTTP(w, r)

			require.Equal(t, w.Code, testCase.expectingStatus)
			if testCase.expectingStatus >= 300 {
				body := w.Body.String()
				var resp struct {
					Status int
					Error  string
				}
				require.NoError(t, json.Unmarshal([]byte(body), &resp))

				require.Equal(t, resp.Error, testCase.respError)
			}
		})
	}
}

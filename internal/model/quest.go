package model

type Quest struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name"`
	Cost int    `json:"cost"`
}

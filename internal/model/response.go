package model

type Response struct {
	Status int         `json:"status"`
	Error  string      `json:"error,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}

func ResponseOk(status int, data interface{}) Response {
	return Response{
		Status: status,
		Data:   data,
	}
}

package model

import "time"

type StatQuest struct {
	Quest
	Time time.Time `json:"time"`
}

type Statistic struct {
	User
	SolvedQuests []*StatQuest `json:"solvedQuests"`
}

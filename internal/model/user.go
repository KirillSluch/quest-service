package model

type User struct {
	ID      string `json:"id,omitempty"`
	Name    string `json:"name"`
	Balance int    `json:"balance,omitempty"`
}

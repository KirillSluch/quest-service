package service

import (
	"context"
	"task-manager/internal/model"
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=QuestService
type QuestService interface {
	// Create new quest by name and cost
	Create(ctx context.Context, newQuest *model.Quest) (*model.Quest, error)
}

package quest

import (
	"context"
	"task-manager/internal/model"
)

func (service *service) Create(ctx context.Context, newQuest *model.Quest) (*model.Quest, error) {
	user, err := service.repository.Save(ctx, newQuest)
	if err != nil {
		return nil, err
	}

	return user, nil
}

package quest

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"task-manager/internal/model"
	repositoryMocks "task-manager/internal/storage/mocks"
	"testing"
)

func TestQuestService_Create(t *testing.T) {
	cases := []struct {
		name      string
		questId   string
		questName string
		cost      int
		respError string
		mockError error
	}{
		{
			name:      "Success",
			questId:   uuid.New().String(),
			questName: "Testing",
			cost:      0,
		},
		{
			name:      "DB Error",
			questId:   uuid.New().String(),
			questName: "Kirill",
			cost:      0,
			mockError: errors.New("some db problems"),
			respError: "some db problems",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			questRepoMock := repositoryMocks.NewQuestRepository(t)
			if testCase.mockError == nil && testCase.respError == "" {
				questRepoMock.
					On(
						"Save",
						context.Background(),
						&model.Quest{
							Name: testCase.questName,
							Cost: testCase.cost,
						},
					).
					Return(
						&model.Quest{
							ID:   testCase.questId,
							Name: testCase.questName,
							Cost: testCase.cost,
						},
						nil,
					)
			} else {
				questRepoMock.
					On(
						"Save",
						context.Background(),
						&model.Quest{
							Name: testCase.questName,
							Cost: testCase.cost,
						},
					).
					Return(
						nil,
						testCase.mockError,
					)
			}

			service := NewService(questRepoMock)
			createdUser, err := service.Create(context.Background(), &model.Quest{
				Name: testCase.questName,
				Cost: testCase.cost,
			})
			if testCase.respError == "" {
				require.NoError(t, err)

				require.Equal(t, testCase.questId, createdUser.ID)
				require.Equal(t, testCase.questName, createdUser.Name)
				require.Equal(t, testCase.cost, createdUser.Cost)
			} else {
				require.Error(t, err)
				require.Equal(t, err.Error(), testCase.respError)
			}
		})
	}

}

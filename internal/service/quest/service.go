package quest

import (
	"task-manager/internal/storage"
)

type service struct {
	repository storage.QuestRepository
}

func NewService(repository storage.QuestRepository) *service {
	return &service{repository: repository}
}

package service

import (
	"context"
	"errors"
	"task-manager/internal/model"
)

var (
	ErrUserNotFound          = errors.New("user not found")
	ErrUserOrQuestNotFound   = errors.New("user or quest not found")
	ErrQuestAlreadyCompleted = errors.New("quest already completed")
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=UserService
type UserService interface {
	// GetStatistics returns user and its history by id
	GetStatistics(ctx context.Context, userId string) (*model.Statistic, error)

	// RegisterQuest mark quest completed by user and append points to last
	RegisterQuest(ctx context.Context, userId string, questId string) error

	// Create new user by name and balance
	Create(ctx context.Context, newUser *model.User) (*model.User, error)
}

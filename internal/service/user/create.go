package user

import (
	"context"
	"task-manager/internal/model"
)

func (service *service) Create(ctx context.Context, newUser *model.User) (*model.User, error) {
	user, err := service.userRepository.Save(ctx, newUser)
	if err != nil {
		return nil, err
	}

	return user, nil
}

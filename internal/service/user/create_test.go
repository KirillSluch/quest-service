package user

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"task-manager/internal/model"
	repositoryMocks "task-manager/internal/storage/mocks"
	"testing"
)

func TestUserService_Create(t *testing.T) {
	cases := []struct {
		name      string
		userId    string
		userName  string
		balance   int
		respError string
		mockError error
	}{
		{
			name:     "Success",
			userId:   uuid.New().String(),
			userName: "Kirill",
			balance:  0,
		},
		{
			name:      "DB Error",
			userId:    uuid.New().String(),
			userName:  "Kirill",
			balance:   0,
			mockError: errors.New("some db problems"),
			respError: "some db problems",
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userRepoMock := repositoryMocks.NewUserRepository(t)
			if testCase.mockError == nil && testCase.respError == "" {
				userRepoMock.
					On(
						"Save",
						context.Background(),
						&model.User{
							Name:    testCase.userName,
							Balance: testCase.balance,
						},
					).
					Return(
						&model.User{
							ID:      testCase.userId,
							Name:    testCase.userName,
							Balance: testCase.balance,
						},
						nil,
					)
			} else {
				userRepoMock.
					On(
						"Save",
						context.Background(),
						&model.User{
							Name:    testCase.userName,
							Balance: testCase.balance,
						},
					).
					Return(
						nil,
						testCase.mockError,
					)
			}

			service := NewService(userRepoMock)
			createdUser, err := service.Create(context.Background(), &model.User{
				Name:    testCase.userName,
				Balance: testCase.balance,
			})
			if testCase.respError == "" {
				require.NoError(t, err)

				require.Equal(t, testCase.userId, createdUser.ID)
				require.Equal(t, testCase.userName, createdUser.Name)
				require.Equal(t, testCase.balance, createdUser.Balance)
			} else {
				require.Error(t, err)
				require.Equal(t, err.Error(), testCase.respError)
			}
		})
	}

}

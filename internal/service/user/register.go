package user

import (
	"context"
	"errors"
	userService "task-manager/internal/service"
	"task-manager/internal/storage"
)

func (service *service) RegisterQuest(ctx context.Context, userId, questId string) error {
	if userId == "" || questId == "" {
		return userService.ErrUserOrQuestNotFound
	}

	err := service.userRepository.RegisterQuest(ctx, userId, questId)

	if err != nil {
		if errors.Is(err, storage.ErrUserOrQuestNotFound) {
			return userService.ErrUserOrQuestNotFound
		} else if errors.Is(err, storage.ErrRegisteredAlready) {
			return userService.ErrQuestAlreadyCompleted
		}
		return err
	}

	return nil
}

package user

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	serviceI "task-manager/internal/service"
	"task-manager/internal/storage"
	repositoryMocks "task-manager/internal/storage/mocks"
	"testing"
)

func TestUserService_RegisterQuest(t *testing.T) {
	cases := []struct {
		name      string
		userId    string
		questId   string
		respError string
		mockError error
	}{
		{
			name:    "Success",
			userId:  uuid.New().String(),
			questId: uuid.New().String(),
		},
		{
			name:      "DB Error",
			userId:    uuid.New().String(),
			questId:   uuid.New().String(),
			mockError: errors.New("some db problems"),
			respError: "some db problems",
		},
		{
			name:      "User Or Quest Not Found",
			userId:    uuid.New().String(),
			questId:   uuid.New().String(),
			mockError: storage.ErrUserOrQuestNotFound,
			respError: serviceI.ErrUserOrQuestNotFound.Error(),
		},
		{
			name:      "User Not Found",
			userId:    "",
			questId:   uuid.New().String(),
			mockError: storage.ErrUserOrQuestNotFound,
			respError: serviceI.ErrUserOrQuestNotFound.Error(),
		},
		{
			name:      "Quest Not Found",
			userId:    uuid.New().String(),
			questId:   "",
			mockError: storage.ErrUserOrQuestNotFound,
			respError: serviceI.ErrUserOrQuestNotFound.Error(),
		},
		{
			name:      "Quest Already Completed",
			userId:    uuid.New().String(),
			questId:   uuid.New().String(),
			mockError: storage.ErrRegisteredAlready,
			respError: serviceI.ErrQuestAlreadyCompleted.Error(),
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userRepoMock := repositoryMocks.NewUserRepository(t)
			if testCase.mockError == nil && testCase.respError == "" {
				userRepoMock.
					On(
						"RegisterQuest",
						context.Background(),
						testCase.userId,
						testCase.questId,
					).
					Return(
						nil,
					)
			} else if testCase.userId != "" && testCase.questId != "" {
				userRepoMock.
					On(
						"RegisterQuest",
						context.Background(),
						testCase.userId,
						testCase.questId,
					).
					Return(
						testCase.mockError,
					)
			}

			service := NewService(userRepoMock)
			err := service.RegisterQuest(context.Background(), testCase.userId, testCase.questId)

			if testCase.respError == "" {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
				require.Equal(t, err.Error(), testCase.respError)
			}
		})
	}

}

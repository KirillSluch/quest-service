package user

import (
	"task-manager/internal/storage"
)

type service struct {
	userRepository storage.UserRepository
}

func NewService(
	userRepository storage.UserRepository,
) *service {
	return &service{
		userRepository: userRepository,
	}
}

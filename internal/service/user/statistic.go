package user

import (
	"context"
	"task-manager/internal/model"
	serviceI "task-manager/internal/service"
)

func (service *service) GetStatistics(ctx context.Context, userId string) (*model.Statistic, error) {
	user, err := service.userRepository.GetById(ctx, userId)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, serviceI.ErrUserNotFound
	}

	stat, err := service.userRepository.GetStatistic(ctx, userId)
	if err != nil {
		return nil, err
	}

	if stat == nil {
		return nil, serviceI.ErrUserNotFound
	}

	return &model.Statistic{
		User:         *user,
		SolvedQuests: stat,
	}, nil
}

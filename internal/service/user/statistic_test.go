package user

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"task-manager/internal/model"
	serviceI "task-manager/internal/service"
	repositoryMocks "task-manager/internal/storage/mocks"
	"testing"
)

func TestUserService_GetStatistics(t *testing.T) {
	cases := []struct {
		name      string
		userId    string
		userName  string
		balance   int
		respError string
		mockError error
	}{
		{
			name:     "Success",
			userId:   uuid.New().String(),
			userName: "Kirill",
			balance:  50,
		},
		{
			name:      "User Not Found",
			userId:    uuid.New().String(),
			mockError: serviceI.ErrUserNotFound,
			respError: serviceI.ErrUserNotFound.Error(),
		},
	}

	for _, testCase := range cases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			userRepoMock := repositoryMocks.NewUserRepository(t)
			if testCase.mockError == nil && testCase.respError == "" {
				userRepoMock.
					On("GetById", context.Background(), testCase.userId).
					Return(&model.User{
						ID:      testCase.userId,
						Name:    testCase.userName,
						Balance: testCase.balance,
					}, nil)
				userRepoMock.On("GetStatistic", context.Background(), testCase.userId).
					Return(make([]*model.StatQuest, 0), nil)
			} else if errors.Is(testCase.mockError, serviceI.ErrUserNotFound) {
				userRepoMock.
					On("GetById", context.Background(), testCase.userId).
					Return(nil, nil)
			}

			service := NewService(userRepoMock)
			stat, err := service.GetStatistics(context.Background(), testCase.userId)

			if testCase.respError == "" {
				require.NoError(t, err)
				require.Equal(t, stat.ID, testCase.userId)
				require.Equal(t, stat.Name, testCase.userName)
				require.Equal(t, stat.Balance, testCase.balance)
			} else {
				require.Error(t, err)
				require.Equal(t, err.Error(), testCase.respError)
			}
		})
	}

}

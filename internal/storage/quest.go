package storage

import (
	"context"
	"task-manager/internal/model"
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=QuestRepository
type QuestRepository interface {
	Save(ctx context.Context, newQuest *model.Quest) (*model.Quest, error)
}

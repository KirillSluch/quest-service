package quest

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
)

type repository struct {
	dbpool *pgxpool.Pool
}

func NewRepository(context context.Context, pool *pgxpool.Pool) (*repository, error) {
	_, err := pool.Exec(context, "CREATE TABLE IF NOT EXISTS quests(id CHAR(36) PRIMARY KEY, name VARCHAR(50), cost INT)")
	if err != nil {
		return nil, err
	}

	return &repository{dbpool: pool}, nil
}

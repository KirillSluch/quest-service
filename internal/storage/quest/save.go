package quest

import (
	"context"
	"github.com/google/uuid"
	"task-manager/internal/model"
)

func (repository *repository) Save(ctx context.Context, newQuest *model.Quest) (*model.Quest, error) {
	if len(newQuest.ID) == 0 {
		newQuest.ID = uuid.New().String()
	}

	_, err := repository.dbpool.Exec(
		ctx,
		"INSERT INTO quests(id, name, cost) VALUES($1,$2,$3)",
		newQuest.ID,
		newQuest.Name,
		newQuest.Cost,
	)
	if err != nil {
		return nil, err
	}

	return newQuest, nil
}

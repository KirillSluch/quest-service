package storage

import (
	"context"
	"errors"
	"task-manager/internal/model"
)

var (
	ErrUserOrQuestNotFound = errors.New("user or quest not exists")
	ErrRegisteredAlready   = errors.New("this registration already exists")
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=UserRepository
type UserRepository interface {
	Save(ctx context.Context, user *model.User) (*model.User, error)
	RegisterQuest(ctx context.Context, userId string, questId string) error
	GetStatistic(ctx context.Context, userId string) ([]*model.StatQuest, error)
	GetById(ctx context.Context, userId string) (*model.User, error)
}

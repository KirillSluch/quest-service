package user

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	"task-manager/internal/model"
)

func (repository *repository) GetById(ctx context.Context, userId string) (*model.User, error) {
	row := repository.dbpool.QueryRow(ctx, "SELECT * FROM users WHERE id = $1", userId)

	var (
		id    string
		name  string
		score int
	)

	err := row.Scan(&id, &name, &score)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &model.User{
		ID:      id,
		Name:    name,
		Balance: score,
	}, nil
}

package user

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5/pgconn"
	"task-manager/internal/storage"
)

func (repository *repository) RegisterQuest(ctx context.Context, userId string, questId string) error {
	tx, err := repository.dbpool.Begin(ctx)
	if err != nil {
		_ = tx.Rollback(ctx)
		return err
	}
	_, err = tx.Exec(
		ctx,
		"INSERT INTO statistic(user_id, quest_id) VALUES($1,$2)",
		userId,
		questId,
	)

	if err != nil {
		_ = tx.Rollback(ctx)
		pgErr := &pgconn.PgError{}
		if errors.As(err, &pgErr) {
			if err.(*pgconn.PgError).Code == "23503" {
				return storage.ErrUserOrQuestNotFound
			} else if err.(*pgconn.PgError).Code == "23505" {
				return storage.ErrRegisteredAlready
			} else {
				return err
			}
		}
		return err
	}

	_, err = tx.Exec(
		ctx,
		"UPDATE users AS u SET balance = u.balance + (SELECT cost FROM quests AS q WHERE q.id = $1) WHERE u.id = $2",
		questId,
		userId,
	)
	if err != nil {
		_ = tx.Rollback(ctx)
		return err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return err
	}
	return nil
}

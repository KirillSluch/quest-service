package user

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
)

type repository struct {
	dbpool *pgxpool.Pool
}

func NewRepository(context context.Context, pool *pgxpool.Pool) (*repository, error) {
	_, err := pool.Exec(
		context,
		"CREATE TABLE IF NOT EXISTS users(id CHAR(36) PRIMARY KEY, name VARCHAR(50), balance INT default 0)",
	)
	if err != nil {
		return nil, err
	}

	_, err = pool.Exec(
		context,
		`CREATE TABLE IF NOT EXISTS statistic(
    			user_id CHAR(36),
    			quest_id CHAR(36),
    			time TIMESTAMP NOT NULL DEFAULT current_timestamp,
    			FOREIGN KEY (user_id) references users (id),
    			FOREIGN KEY (quest_id) references quests (id),
    			UNIQUE (user_id, quest_id)
            )`,
	)
	if err != nil {
		return nil, err
	}

	return &repository{dbpool: pool}, nil
}

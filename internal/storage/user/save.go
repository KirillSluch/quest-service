package user

import (
	"context"
	"github.com/google/uuid"
	"task-manager/internal/model"
)

func (repository *repository) Save(ctx context.Context, user *model.User) (*model.User, error) {
	if len(user.ID) == 0 {
		user.ID = uuid.New().String()
	}
	user.Balance = 0

	_, err := repository.dbpool.Exec(
		ctx,
		"INSERT INTO users(id, name, balance) VALUES($1,$2,$3)",
		user.ID,
		user.Name,
		user.Balance,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

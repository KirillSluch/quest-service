package user

import (
	"context"
	"task-manager/internal/model"
	"time"
)

func (repository *repository) GetStatistic(ctx context.Context, userId string) ([]*model.StatQuest, error) {
	rows, err := repository.dbpool.Query(
		ctx,
		"SELECT id, name, cost, time FROM (SELECT quest_id, time FROM statistic st WHERE user_id = $1) AS sqi INNER JOIN quests q ON sqi.quest_id = q.id",
		userId,
	)
	if err != nil {
		return nil, err
	}

	res := make([]*model.StatQuest, 0)

	var (
		id, name string
		cost     int
		statTime time.Time
	)

	for rows.Next() {
		err := rows.Scan(&id, &name, &cost, &statTime)
		if err != nil {
			return nil, err
		}
		res = append(res, &model.StatQuest{
			Quest: model.Quest{
				ID:   id,
				Name: name,
				Cost: cost,
			},
			Time: statTime,
		})
	}

	return res, nil
}

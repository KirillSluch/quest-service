package tests

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/url"
	"task-manager/internal/model"
	"testing"

	"github.com/gavv/httpexpect/v2"
)

const (
	host  = "localhost:8082"
	token = "a1b2c33d4e5f6g7h8i9jakblc"
)

var (
	u = url.URL{
		Scheme: "http",
		Host:   host,
	}
)

func TestQuestService_SuccessFlow(t *testing.T) {
	e := httpexpect.Default(t, u.String())

	var user struct {
		Data   model.User `json:"data"`
		Status int        `json:"status"`
	}

	e.POST("/user").
		WithJSON(model.User{
			Name:    gofakeit.Name(),
			Balance: 0,
		}).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusCreated).
		JSON().Object().
		ContainsKey("data").
		Decode(&user)

	var quest struct {
		Data   model.Quest `json:"data"`
		Status int         `json:"status"`
	}

	e.POST("/quest").
		WithJSON(model.Quest{
			Name: "Quest 1",
			Cost: gofakeit.IntRange(10, 1000),
		}).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusCreated).
		JSON().Object().
		Decode(&quest)

	e.POST(fmt.Sprintf("/user/%s/quest/%s", user.Data.ID, quest.Data.ID)).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusAccepted)

	var response struct {
		Data   model.Statistic `json:"data"`
		Status int             `json:"status"`
	}

	userTotal := quest.Data.Cost

	e.GET(fmt.Sprintf("/user/%s/history", user.Data.ID)).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		Decode(&response)

	require.Equal(t, userTotal, response.Data.Balance)
	require.Equal(t, 1, len(response.Data.SolvedQuests))

	e.POST("/quest").
		WithJSON(model.Quest{
			Name: "Quest 2",
			Cost: gofakeit.IntRange(10, 1000),
		}).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusCreated).
		JSON().Object().
		Decode(&quest)

	userTotal += quest.Data.Cost

	e.POST(fmt.Sprintf("/user/%s/quest/%s", user.Data.ID, quest.Data.ID)).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusAccepted)

	e.GET(fmt.Sprintf("/user/%s/history", user.Data.ID)).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		Decode(&response)

	require.Equal(t, userTotal, response.Data.Balance)
	require.Equal(t, 2, len(response.Data.SolvedQuests))
}

func TestQuestService_QuestRegisterFail(t *testing.T) {
	e := httpexpect.Default(t, u.String())

	e.POST(fmt.Sprintf("/user/%s/quest/%s", gofakeit.UUID(), gofakeit.UUID())).
		WithHeader("Authorization", token).
		Expect().
		Status(http.StatusNotFound)
}

func TestQuestService_StatisticNotFound(t *testing.T) {
	e := httpexpect.Default(t, u.String())

	e.GET(fmt.Sprintf("/user/%s/history", gofakeit.UUID())).
		Expect().
		Status(http.StatusNotFound)
}

func TestQuestService_Unauthorized(t *testing.T) {
	e := httpexpect.Default(t, u.String())

	e.POST("/quest").
		WithJSON(model.Quest{
			Name: "Quest 1",
			Cost: gofakeit.IntRange(10, 1000),
		}).
		Expect().
		Status(http.StatusUnauthorized)
}

func TestQuestService_Forbidden(t *testing.T) {
	e := httpexpect.Default(t, u.String())

	e.POST("/quest").
		WithJSON(model.Quest{
			Name: "Quest 1",
			Cost: gofakeit.IntRange(10, 1000),
		}).
		WithHeader("Authorization", token+"f").
		Expect().
		Status(http.StatusForbidden)
}
